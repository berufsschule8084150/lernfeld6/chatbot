/**
 *
 */
package chatbotitsolution.start;

import java.util.Scanner;

import chatbotitsolution.lib.ChatbotStart;

/**
 *
 */
public final class NotMain {

	public static void main(final String[] args) {
		final var sc = new Scanner(System.in);

		new ChatbotStart().start(sc);

		sc.close();
	}
}
