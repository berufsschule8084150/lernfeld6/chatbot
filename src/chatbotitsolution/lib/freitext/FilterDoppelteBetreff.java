/**
 *
 */
package chatbotitsolution.lib.freitext;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class FilterDoppelteBetreff implements Iterable<ProblemKategorien> {

	private final Collection<ProblemKategorien> problemKategorien;
	private final Set<String> existingRueckgaben = new HashSet<>();

	public FilterDoppelteBetreff(final Collection<ProblemKategorien> problemKategorien) {
		this.problemKategorien = problemKategorien;
	}

	@Override
	public Iterator<ProblemKategorien> iterator() {
		return this.problemKategorien.stream().filter(objekt -> this.existingRueckgaben.add(objekt.betreff()))
				.collect(Collectors.toList()).iterator();
	}

}
