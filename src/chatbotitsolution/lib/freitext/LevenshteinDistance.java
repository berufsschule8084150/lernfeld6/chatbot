/**
 *
 */
package chatbotitsolution.lib.freitext;

/**
 *
 */
public final class LevenshteinDistance {

	private final String str1;
	private final String str2;
	private final int[][] distanceMatrix;

	public LevenshteinDistance(final String str1, final String str2) {
		this.str1 = str1;
		this.str2 = str2;
		this.distanceMatrix = new int[str1.length() + 1][str2.length() + 1];
	}

	private void initializeMatrix() {
		for (var i = 0; i <= this.str1.length(); i++)
			for (var j = 0; j <= this.str2.length(); j++)
				if (i == 0)
					this.distanceMatrix[i][j] = j;
				else if (j == 0)
					this.distanceMatrix[i][j] = i;
	}

	private void calculateDistance() {
		for (var i = 1; i <= this.str1.length(); i++)
			for (var j = 1; j <= this.str2.length(); j++)
				if (this.str1.charAt(i - 1) == this.str2.charAt(j - 1))
					this.distanceMatrix[i][j] = this.distanceMatrix[i - 1][j - 1];
				else
					this.distanceMatrix[i][j] = 1 + this.min(this.distanceMatrix[i - 1][j], // Delete
							this.distanceMatrix[i][j - 1], // Insert
							this.distanceMatrix[i - 1][j - 1] // Substitute
					);
	}

	private int min(final int a, final int b, final int c) {
		return Math.min(Math.min(a, b), c);
	}

	public int distance() {
		this.initializeMatrix();
		this.calculateDistance();
		return this.distanceMatrix[this.str1.length()][this.str2.length()];
	}
}
