/**
 *
 */
package chatbotitsolution.lib.freitext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;
import chatbotitsolution.lib.auswahl.kategorien.hardware.Hardware;
import chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.Computer;
import chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.Maus;
import chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.Tastatur;
import chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.drucker.Drucker;
import chatbotitsolution.lib.auswahl.kategorien.internet.Internet;
import chatbotitsolution.lib.auswahl.kategorien.lieferung.Lieferung;
import chatbotitsolution.lib.auswahl.kategorien.software.Software;
import chatbotitsolution.lib.auswahl.kategorien.sontiges.Sonstiges;
import chatbotitsolution.lib.freitext.uebereinstimmung.EineUebereinstimmung;
import chatbotitsolution.lib.freitext.uebereinstimmung.MehrAlsEineUebereinstimmung;
import chatbotitsolution.lib.freitext.uebereinstimmung.NullUebereinstimmung;

/**
 *
 */
public final class Freitext implements BiConsumer<String, Scanner> {

	private final Iterable<ProblemKategorien> problemKategorien = Arrays.asList(new Hardware(), new Software(),
			new Internet(), new Lieferung(), new Sonstiges(), new Maus(), new Tastatur(), new Computer(), new Drucker(),
			new Internet());
	private final Iterable<BiConsumer<List<ProblemKategorien>, Scanner>> uebereinstimmungen = Arrays
			.asList(new NullUebereinstimmung(), new EineUebereinstimmung(), new MehrAlsEineUebereinstimmung());

	@Override
	public void accept(final String t, final Scanner u) {
		if (!"0".equals(t)) {
			final var inputs = Arrays.asList(t.split(" ")).stream().map(a -> a.replaceAll("[,\\.]", ""))
					.collect(Collectors.toList());
			final var uebereinstimmendeProblemkategorien = new ArrayList<ProblemKategorien>();

			for (final String input : inputs)
				for (final ProblemKategorien problemKategorie : this.problemKategorien) {
					final var betreff = problemKategorie.betreff();
					if (new LevenshteinDistance(input, betreff).distance() <= 2)
						uebereinstimmendeProblemkategorien.add(problemKategorie);
				}
			for (final BiConsumer<List<ProblemKategorien>, Scanner> uebereinstimmung : this.uebereinstimmungen)
				uebereinstimmung.accept(StreamSupport
						.stream(new FilterDoppelteBetreff(uebereinstimmendeProblemkategorien).spliterator(), false)
						.collect(Collectors.toList()), u);
		}

	}

}
