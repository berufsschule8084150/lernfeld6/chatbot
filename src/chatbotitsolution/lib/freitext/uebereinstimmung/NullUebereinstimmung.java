/**
 *
 */
package chatbotitsolution.lib.freitext.uebereinstimmung;

import java.util.List;
import java.util.Scanner;
import java.util.function.BiConsumer;

import chatbotitsolution.lib.auswahl.AufforderungZurEingabe;
import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class NullUebereinstimmung implements BiConsumer<List<ProblemKategorien>, Scanner> {

	@Override
	public void accept(final List<ProblemKategorien> t, final Scanner u) {
		if (t.size() == 0) {
			System.out.println("Wir konnten keine Uebereinstimmung finden. Try Again.");
			new AufforderungZurEingabe().auffordern(u);
		}
	}

}
