/**
 *
 */
package chatbotitsolution.lib.freitext.uebereinstimmung;

import java.util.List;
import java.util.Scanner;
import java.util.function.BiConsumer;

import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class EineUebereinstimmung implements BiConsumer<List<ProblemKategorien>, Scanner> {

	@Override
	public void accept(final List<ProblemKategorien> t, final Scanner u) {
		if (t.size() == 1)
			t.get(0).choose(u);
	}

}
