/**
 *
 */
package chatbotitsolution.lib.freitext.uebereinstimmung;

import java.util.List;
import java.util.Scanner;
import java.util.function.BiConsumer;

import chatbotitsolution.lib.auswahl.ProblemKategorieAuswahl;
import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class MehrAlsEineUebereinstimmung implements BiConsumer<List<ProblemKategorien>, Scanner> {

	@Override
	public void accept(final List<ProblemKategorien> t, final Scanner u) {
		if (t.size() > 1) {
			System.out.println("Es wurden mehrere passende Problem Kategorien gefunden.");
			new ProblemKategorieAuswahl(t).wahle(u);
		}
	}
}
