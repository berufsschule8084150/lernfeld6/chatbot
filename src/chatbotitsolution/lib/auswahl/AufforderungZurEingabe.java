/**
 *
 */
package chatbotitsolution.lib.auswahl;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.BiConsumer;

import chatbotitsolution.lib.freitext.Freitext;

/**
 *
 */
public final class AufforderungZurEingabe {

	private final Iterable<BiConsumer<String, Scanner>> starter;

	public AufforderungZurEingabe() {
		this.starter = Arrays.asList(new Freitext(), new Auswahl());
	}

	public void auffordern(final Scanner sc) {
		System.out.println(
				"Sie können entweder Freitext eingeben, dann wird versucht ihre Problem Kategorie herauszufinden");
		System.out.println("Oder 0, dann wird Ihnen eine Auswahl an Problem Kategorien gezeigt und Sie wählen aus");

		final var userEingabe = sc.nextLine().strip();

		for (final BiConsumer<String, Scanner> biConsumer : this.starter)
			biConsumer.accept(userEingabe, sc);
	}

}
