/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien.software;

import java.util.Scanner;

import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class Software implements ProblemKategorien {

	@Override
	public String betreff() {
		return "Software";
	}

	@Override
	public void choose(final Scanner t) {
		System.out.println("Sie werden weitergeleitet");
		System.out.println();
		System.out.println("Info an mitarbeitende Person: Problem mit Software");
	}

}
