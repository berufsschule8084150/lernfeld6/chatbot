/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien;

import java.util.Arrays;
import java.util.Iterator;

import chatbotitsolution.lib.auswahl.kategorien.hardware.Hardware;
import chatbotitsolution.lib.auswahl.kategorien.internet.Internet;
import chatbotitsolution.lib.auswahl.kategorien.lieferung.Lieferung;
import chatbotitsolution.lib.auswahl.kategorien.software.Software;
import chatbotitsolution.lib.auswahl.kategorien.sontiges.Sonstiges;

/**
 *
 */
public final class Kategorien implements Iterable<ProblemKategorien> {

	@Override
	public Iterator<ProblemKategorien> iterator() {
		return Arrays.asList(new Hardware(), new Software(), new Internet(), new Lieferung(), new Sonstiges())
				.iterator();
	}

}
