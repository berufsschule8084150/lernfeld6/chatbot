/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien.lieferung;

import java.util.Scanner;

import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class Lieferung implements ProblemKategorien {

	@Override
	public String betreff() {
		return "Lieferung";
	}

	@Override
	public void choose(final Scanner t) {
		System.out.println("Sie werden weiter geleitet zur Lieferungsabteilung");
		System.out.println();
		System.out.println("Info an mitarbeitende Person: Problem mit Lager");
	}

}
