/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien.internet;

import java.util.Scanner;

import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class NichtVorhanden implements ProblemKategorien {

	@Override
	public String betreff() {
		return "Nicht vorhanden";
	}

	@Override
	public void choose(final Scanner t) {
		System.out.println("Sie werden weitergeleitet");
		System.out.println();
		System.out.println("Info an mitarbeitende Person: Problem mit Internet, nicht vorhanden");
	}

}
