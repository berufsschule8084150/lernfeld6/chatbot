/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien.internet;

import java.util.Arrays;
import java.util.Scanner;

import chatbotitsolution.lib.auswahl.ProblemKategorieAuswahl;
import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class Internet implements ProblemKategorien {

	@Override
	public String betreff() {
		return "Internet";
	}

	@Override
	public void choose(final Scanner t) {
		new ProblemKategorieAuswahl(Arrays.asList(new Langsam(), new NichtVorhanden())).wahle(t);
	}

}
