/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien.hardware;

import java.util.Arrays;
import java.util.Scanner;

import chatbotitsolution.lib.auswahl.ProblemKategorieAuswahl;
import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;
import chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.Computer;
import chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.Maus;
import chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.Sonstiges;
import chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.Tastatur;
import chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.drucker.Drucker;

/**
 *
 */
public final class Hardware implements ProblemKategorien {

	@Override
	public String betreff() {
		return "Hardware";
	}

	@Override
	public void choose(final Scanner t) {
		new ProblemKategorieAuswahl(
				Arrays.asList(new Maus(), new Tastatur(), new Computer(), new Drucker(), new Sonstiges())).wahle(t);

	}

}
