/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.drucker;

import java.util.Scanner;

import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class GehtNichtAn implements ProblemKategorien {

	@Override
	public String betreff() {
		return "Geht nicht an";
	}

	@Override
	public void choose(final Scanner t) {
		System.out.println("Sie werden weitergeleitet");
		System.out.println();
		System.out.println("Info an mitarbeitende Person: Problem mit Hardware Drucker, geht nicht an");
	}
}
