/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien.hardware.geraete.drucker;

import java.util.Arrays;
import java.util.Scanner;

import chatbotitsolution.lib.auswahl.ProblemKategorieAuswahl;
import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class Drucker implements ProblemKategorien {

	@Override
	public String betreff() {
		return "Drucker";
	}

	@Override
	public void choose(final Scanner t) {
		new ProblemKategorieAuswahl(Arrays.asList(new GehtNichtAn(), new DrucktNicht())).wahle(t);
	}

}
