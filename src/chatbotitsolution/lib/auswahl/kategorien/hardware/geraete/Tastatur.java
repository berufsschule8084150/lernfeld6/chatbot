/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien.hardware.geraete;

import java.util.Scanner;

import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class Tastatur implements ProblemKategorien {

	@Override
	public String betreff() {
		return "Tastatur";
	}

	@Override
	public void choose(final Scanner t) {
		System.out.println("Sie werden weitergeleitet");
		System.out.println();
		System.out.println("Info an mitarbeitende Person: Problem mit Hardware Tastatur");
	}

}
