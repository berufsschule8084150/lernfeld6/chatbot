/**
 *
 */
package chatbotitsolution.lib.auswahl.kategorien;

import java.util.Scanner;

/**
 *
 */
public interface ProblemKategorien {

	String betreff();

	void choose(Scanner t);
}
