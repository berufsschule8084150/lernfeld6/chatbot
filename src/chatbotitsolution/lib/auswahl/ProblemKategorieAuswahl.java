/**
 *
 */
package chatbotitsolution.lib.auswahl;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import chatbotitsolution.lib.auswahl.kategorien.Kategorien;
import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;

/**
 *
 */
public final class ProblemKategorieAuswahl {

	private final Iterable<ProblemKategorien> problemKategorien;

	public ProblemKategorieAuswahl() {
		this(new Kategorien());
	}

	public ProblemKategorieAuswahl(final Iterable<ProblemKategorien> problemKategorien) {
		this.problemKategorien = problemKategorien;
	}

	public void wahle(final Scanner sc) {
		System.out.println("Wählen sie bitte die passende Kategorie aus:");
		var counter = 1;

		for (final ProblemKategorien problemKategorie : this.problemKategorien) {
			System.out.println("[" + counter + "]" + " " + problemKategorie.betreff());
			counter++;
		}
		final var userInput = this.getNumber(sc, counter - 1);

		final var problemKategorien = StreamSupport.stream(this.problemKategorien.spliterator(), false)
				.collect(Collectors.toList());

		problemKategorien.get(userInput - 1).choose(sc);

	}

	private Integer getNumber(final Scanner sc, final Integer counter) {
		try {
			final var userInput = Integer.valueOf(sc.nextLine());
			if (userInput > counter || userInput < 1) {
				System.out.println("Die eingegebene Zahl steht nicht zur Auswahl, try again:");
				return this.getNumber(sc, counter);
			}
			return userInput;
		} catch (final NumberFormatException e) {
			System.out.println("This is not a number try again");
			return this.getNumber(sc, counter);
		}
	}
}
