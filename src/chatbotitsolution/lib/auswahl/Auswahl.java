/**
 *
 */
package chatbotitsolution.lib.auswahl;

import java.util.Scanner;
import java.util.function.BiConsumer;

/**
 *
 */
public class Auswahl implements BiConsumer<String, Scanner> {

	@Override
	public void accept(final String t, final Scanner u) {
		if ("0".equals(t))
			new ProblemKategorieAuswahl().wahle(u);
	}

}
