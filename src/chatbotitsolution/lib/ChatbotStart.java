/**
 *
 */
package chatbotitsolution.lib;

import java.util.Scanner;

import chatbotitsolution.lib.auswahl.AufforderungZurEingabe;

/**
 *
 */
public final class ChatbotStart {

	public void start(final Scanner sc) {
		System.out.println("Moin Leute Trymacs hier, wie kann ich ihnen helfen ?");

		new AufforderungZurEingabe().auffordern(sc);
	}
}
