/**
 *
 */
package chatbotitsolution.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import chatbotitsolution.lib.freitext.LevenshteinDistance;

/**
 *
 */
public final class LevenshteinDistanceTest {

	private void testeDistance(final String data1, final String data2, final Integer expected) {
		final var sut = new LevenshteinDistance(data1, data2);

		Assertions.assertEquals(expected, sut.distance());
	}

	@Test
	public void teste1() {
		this.testeDistance("kitten", "sitting", 3);
	}

	@Test
	public void teste2() {
		this.testeDistance("hello", "hola", 3);
	}

	@Test
	public void teste3() {
		this.testeDistance("abc", "abc", 0);
	}

	@Test
	public void teste4() {
		this.testeDistance("cat", "dog", 3);
	}

	@Test
	public void teste5() {
		this.testeDistance("intention", "execution", 5);
	}

	@Test
	public void teste6() {
		this.testeDistance("abcdef", "123abc", 6);
	}

	@Test
	public void teste7() {
		this.testeDistance("book", "back", 2);
	}

	@Test
	public void teste8() {
		this.testeDistance("horse", "ros", 3);
	}

	@Test
	public void teste9() {
		this.testeDistance("openai", "gpt", 5);
	}

	@Test
	public void teste10() {
		this.testeDistance("abc", "", 3);
	}

}
