/**
 *
 */
package chatbotitsolution.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import chatbotitsolution.lib.auswahl.kategorien.ProblemKategorien;
import chatbotitsolution.lib.auswahl.kategorien.hardware.Hardware;
import chatbotitsolution.lib.auswahl.kategorien.software.Software;
import chatbotitsolution.lib.freitext.FilterDoppelteBetreff;

/**
 *
 */
public final class FilterDoppelteBetreffTest {

	@Test
	public void teste1() {
		final var sut = new FilterDoppelteBetreff(new ArrayList<>(Arrays.asList(new Hardware(), new Hardware())));

		final List<ProblemKategorien> come = StreamSupport.stream(sut.spliterator(), false)
				.collect(Collectors.toList());

		Assertions.assertEquals(1, come.size());
	}

	@Test
	public void teste2() {
		final var sut = new FilterDoppelteBetreff(
				new ArrayList<>(Arrays.asList(new Hardware(), new Hardware(), new Software(), new Software())));

		final List<ProblemKategorien> come = StreamSupport.stream(sut.spliterator(), false)
				.collect(Collectors.toList());

		Assertions.assertEquals(2, come.size());
	}

	@Test
	public void teste3() {
		final var sut = new FilterDoppelteBetreff(new ArrayList<>(Arrays.asList(new Hardware(), new Software())));

		final List<ProblemKategorien> come = StreamSupport.stream(sut.spliterator(), false)
				.collect(Collectors.toList());

		Assertions.assertEquals(2, come.size());
	}
}
